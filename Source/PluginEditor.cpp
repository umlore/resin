/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
ResinAudioProcessorEditor::ResinAudioProcessorEditor (ResinAudioProcessor& p) :
        AudioProcessorEditor (&p),
        audioProcessor (p),
        audioFormatFilter (juce::WildcardFileFilter(audioProcessor.getAudioWildcardFormats(), {}, {}))
{
    loadSampleButton.onClick = [&]() { audioProcessor.loadFile(); };
    addAndMakeVisible(loadSampleButton);

    setSize (400, 300);
}

ResinAudioProcessorEditor::~ResinAudioProcessorEditor()
{
}

//==============================================================================
void ResinAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void ResinAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

    loadSampleButton.setBounds(0, 0, getWidth(), getHeight());
}

//load the first suitable audio file in the group of dropped files.
void ResinAudioProcessorEditor::filesDropped(const juce::StringArray& files, int x, int y)
{
    for (auto file : files) {
        if (audioFormatFilter.isFileSuitable(file)) {
            audioProcessor.loadFile(file);
            return;
        }
    }
}

//if there's at least one audio file, we're interested.
bool ResinAudioProcessorEditor::isInterestedInFileDrag(const juce::StringArray& files)
{
    for (auto file : files) {
        if (audioFormatFilter.isFileSuitable(file))
            return true;
    }

    return false;
}
